const limitFunctionCallCount = require('../limitFunctionCallCount.cjs')

function cb(...args){
    console.log(args)
}

try{
    const ans = limitFunctionCallCount(cb,5)
    ans()
    ans()
    ans()
    ans()
    ans()
    console.log(ans())
}
catch(err){
    console.log(err)
}

