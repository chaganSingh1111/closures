const cacheFunction =(cb)=>{
    if (typeof cb !== 'function' ) {
        throw new Error(' ERROR : Invalid Input');
      }


    let cachedObj = {}
    
    return function cbInvoke(...data){
        const key = JSON.stringify(data)
        if(key in cachedObj){
            return cachedObj[key]

        }
        else{
            const ans = cb(...data)
            cachedObj[key] = ans
            return ans
        }
        
    }
    
        
}
module.exports = cacheFunction