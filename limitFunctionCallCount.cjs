const limitFunctionCallCount =(cb,n)=>{
    if (typeof cb !== 'function' || typeof n !== 'number' || n < 0 || n===undefined) {
        throw new Error(' ERROR : Invalid Input');
      }


    let count =0

    return function (...args){
        if(count < n){
            count=count+1
            return cb(...args);
        }
        else{
            return null
        }
    }
}
module.exports = limitFunctionCallCount